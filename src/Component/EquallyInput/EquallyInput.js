import React from "react";

const EquallyInput = (props) => {
  return (
    <p>
      {props.title}
      <input type="number" value={props.value} onChange={props.onChange} />
      {props.unit}
    </p>
  );
};

export default EquallyInput;
