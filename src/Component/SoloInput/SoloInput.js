import React from "react";
import "./SoloInput.css";

const SoloInput = (props) => {
  return (
    <div className="SoloInput">
      <input
        value={props.value1}
        placeholder="Имя"
        onChange={props.onChangeName}
        type="text"
      />
      <b>
        <input
          value={props.value2}
          placeholder="Сумма"
          onChange={props.onChangeSum}
          type="number"
        />
        KGS
      </b>
      <button onClick={props.removed}>Delete</button>
    </div>
  );
};

export default SoloInput;
