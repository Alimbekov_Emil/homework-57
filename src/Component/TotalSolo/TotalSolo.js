import React from "react";
import "./TotalSolo.css";

const TotalSolo = (props) => {
  if (props.sum === 0) {
    return null;
  }
  const order = props.items.map((item, index) => {
    let percents = parseInt(item.sum * props.percent) / 100;

    return (
      <div className="Person" key={item.name + item.sum + index}>
        {item.name}:
        {parseInt(item.sum) + percents + props.shipping / props.items.length}
        сом
      </div>
    );
  });

  return (
    <div>
      <p>
        Общая сумма:
        {props.sum + props.shipping + (props.sum * props.percent) / 100}
      </p>
      {order}
    </div>
  );
};

export default TotalSolo;
