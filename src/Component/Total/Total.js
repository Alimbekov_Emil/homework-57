import React from "react";

const Total = (props) => {
  if (props.total === 0) {
    return null;
  }
  return (
    <div>
      <p>Общая Сумма: {props.sum + (props.sum * props.percent) / 100}</p>
      <p> Количество человек: {props.mens}</p>
      <p>Каждый платит по: {props.total}</p>
    </div>
  );
};

export default Total;
