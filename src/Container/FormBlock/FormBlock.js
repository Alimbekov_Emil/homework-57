import React, { useState } from "react";
import Equally from "../Equally/Equally";
import Solo from "../Solo/Solo";
import "./FormBlock.css";

const FormBlock = () => {
  const [mode, setMode] = useState("second");

  const onRadionChange = (e) => {
    setMode(e.target.value);
  };

  return (
    <div className="FormBlock">
        Сумма заказа считается:
      <p>
        <input
          type="radio"
          name="options"
          value="first"
          onChange={onRadionChange}
          checked={mode === "first"}
        />
        Поровну между всеми участниками
      </p>
      <p>
        <input
          type="radio"
          name="options"
          value="second"
          onChange={onRadionChange}
          checked={mode === "second"}
        />
        Каждому индивидуально
      </p>

      <div>
        {mode === "first" && <Equally />}
        {mode === "second" && <Solo />}
      </div>
    </div>
  );
};

export default FormBlock;
