import React, { useState } from "react";
import EquallyInput from "../../Component/EquallyInput/EquallyInput";
import SoloInput from "../../Component/SoloInput/SoloInput";
import TotalSolo from "../../Component/TotalSolo/TotalSolo";
import "./Solo.css";

const Solo = () => {
  const [items, setItems] = useState([{ name: "", sum: 0 }]);
  const [percent, setPercent] = useState(0);
  const [shipping, setShipping] = useState(0);
  const [total, setTotal] = useState(0);

  const addItem = () => {
    setItems([...items, { name: "", sum: 0 }]);
  };

  const onChangeName = (e, i) => {
    const itemsCopy = [...items];
    const itemCopy = { ...itemsCopy[i] };
    itemCopy.name = e.target.value;
    itemsCopy[i] = itemCopy;

    setItems(itemsCopy);
  };

  const onChangeSum = (e, i) => {
    const itemsCopy = [...items];
    const itemCopy = { ...itemsCopy[i] };
    itemCopy.sum = e.target.value;
    itemsCopy[i] = itemCopy;
    setItems(itemsCopy);
  };

  const removedItem = (i) => {
    const itemsCopy = [...items];
    itemsCopy.splice(i, 1);
    setItems(itemsCopy);
  };

  const onPercentChange = (e) => {
    setPercent(parseInt(e.target.value));
  };
  const onShippingChange = (e) => {
    setShipping(parseInt(e.target.value));
  };

  const getTotal = () => {
    let result = 0;

    items.forEach((item) => {
      result += parseInt(item.sum);
    });

    setTotal(result);
  };

  const person = items.map((item, i) => {
    return (
      <SoloInput
        key={i}
        value1={item.name}
        value2={item.sum}
        onChangeName={(e) => onChangeName(e, i)}
        onChangeSum={(e) => onChangeSum(e, i)}
        removed={() => removedItem(i)}
      />
    );
  });

  return (
    <div className="Solo">
      {person}
      <button onClick={addItem}>Добавить Персону</button>
      <EquallyInput
        title="Процент чаевых"
        onChange={onPercentChange}
        value={percent}
        unit="%"
      />
      <EquallyInput
        title="Доставка"
        onChange={onShippingChange}
        value={shipping}
        unit="сом"
      />
      <TotalSolo sum={total} items={items} percent={percent} shipping={shipping} />
      <button onClick={getTotal}>Расчитать</button>
    </div>
  );
};

export default Solo;
