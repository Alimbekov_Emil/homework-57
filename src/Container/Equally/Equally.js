import React, { useState } from "react";
import EquallyInput from "../../Component/EquallyInput/EquallyInput";
import Total from "../../Component/Total/Total";

import "./Equally.css";

const Equally = () => {
  const [mens, setMens] = useState(0);
  const [sum, setSum] = useState(0);
  const [percent, setPercent] = useState(0);
  const [shipping, setShipping] = useState(0);
  const [totalPrice, setTotalPrice] = useState(0);

  const onMensChange = (e) => {
    setMens(e.target.value);
  };
  const onSumChange = (e) => {
    setSum(parseInt(e.target.value));
  };
  const onPercentChange = (e) => {
    setPercent(e.target.value);
  };
  const onShippingChange = (e) => {
    setShipping(parseInt(e.target.value));
  };

  const total = () => {
    let a = sum * (percent / 100);
    let b = sum + a + shipping;
    let c = b / mens;
    setTotalPrice(Math.ceil(c));
  };

  return (
    <div className="Equally">
      <EquallyInput
        title="Человек"
        onChange={onMensChange}
        value={mens}
        unit="чел."
      />
      <EquallyInput
        title="Сумма Заказа"
        onChange={onSumChange}
        value={sum}
        unit="сом"
      />
      <EquallyInput
        title="Процент чаевых"
        onChange={onPercentChange}
        value={percent}
        unit="%"
      />
      <EquallyInput
        title="Доставка"
        onChange={onShippingChange}
        value={shipping}
        unit="сом"
      />

      <button onClick={total}>Расчитать</button>
      <Total mens={mens} total={totalPrice} sum={sum} percent={percent}/>
    </div>
  );
};

export default Equally;
